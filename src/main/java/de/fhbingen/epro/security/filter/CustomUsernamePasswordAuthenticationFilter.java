package de.fhbingen.epro.security.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import de.fhbingen.epro.error.UsernamePasswordValidationErrors;
import de.fhbingen.epro.web.validation.utils.ValidationUtils;

public class CustomUsernamePasswordAuthenticationFilter extends  UsernamePasswordAuthenticationFilter {
	public CustomUsernamePasswordAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super();
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		String username = null,
			   password = null;
		if (request.getMethod().equals(HttpMethod.OPTIONS.toString())) {
			response.setStatus(HttpStatus.OK.value());
			return null;
		} else if (request.getMethod().equals("POST")) {
			if (verifyHeaderContentType(request) || request.getHeader("Accept") != null)
				response.addHeader("Content-Type", request.getHeader("Accept"));
			
			if (verifyHeaderContentType(request)) {
				try {
					loginBean = requestToLoginBean(request);
				} catch (IOException e) {
					response.setStatus(HttpStatus.BAD_REQUEST.value());
				}
			}
			
			username = obtainUsername(request);			
			password = obtainPassword(request);
			
			UsernamePasswordValidationErrors errors = new UsernamePasswordValidationErrors("user");
			if (username == null || username == "")
				ValidationUtils.rejectBlank(errors, "username", "Field may not be empty");
			if (password == null || password == "")
				ValidationUtils.rejectBlank(errors, "password", "Field may not be empty");
			
			if (errors.hasErrors()) {
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				try {
					response.getWriter()
						.append(convertObjectToJson(ValidationUtils.resolveResponse("user", errors)))
						.flush();
					return null;
				} catch (IOException e) {
					throw new AuthenticationServiceException("Error generating BAD_REQUEST response", e.getCause());
				}			
			}
			
			username = username.toLowerCase().trim();
			password = password.trim();
			
			UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
					username, password);

			setDetails(request, authRequest);
			Authentication authentication = this.getAuthenticationManager().authenticate(authRequest); 
			
			if (authentication.isAuthenticated())
				updateLastLogin(username);
			
			return authentication;
		} else {
			throw new AuthenticationServiceException(
					"HTTP method not supported: " + request.getMethod());
		}
	}
}
