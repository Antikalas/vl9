package de.fhbingen.epro.security.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import de.fhbingen.epro.model.User;
import de.fhbingen.epro.security.encoder.CostumPasswordEncoder;
import de.fhbingen.epro.service.CustomUserDetailsService;
import de.fhbingen.epro.service.UserService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	UserService userService;
	
	@Autowired
	CustomUserDetailsService userRightService;
	
	@Autowired
	CostumPasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
	User user = userService.findByEmail(authentication.getPrincipal().toString());
	if (user == null) {
	throw new UsernameNotFoundException(String.format("Invalid credentials", authentication.getPrincipal()));
	}
	if(!passwordEncoder.matches(authentication.getCredentials().toString(), user.getPassword()))
	throw new BadCredentialsException("Invalid credentials");
	UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(user, null, userRightService.getGrantedAuthorities(user));
	return token;
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}
}
