package de.fhbingen.epro.security.encoder;

import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CostumPasswordEncoder extends PlaintextPasswordEncoder{
	
	public boolean matches(String pw1,String pw2){
		return pw1.equals(pw2);
		
	}

}
