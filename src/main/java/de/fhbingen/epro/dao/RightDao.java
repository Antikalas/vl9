package de.fhbingen.epro.dao;

import java.util.List;

import de.fhbingen.epro.model.Right;



public interface RightDao {

	Right findById(long id);

	void saveRight(Right right);
	
	void deleteRightById(long id);
	
	List<Right> findAllRight();

	Right findRightById(long id);

}
