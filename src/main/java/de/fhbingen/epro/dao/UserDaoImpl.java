package de.fhbingen.epro.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import de.fhbingen.epro.model.User;


@Repository("UserDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Override
	public User findById(long id) {
		return getByKey(id);
	}

	@Override
	public void saveUser(User user) {
		persist(user);
	}


	@Override
	public void deleteUserById(long id) {
		Query query = getSession().createSQLQuery("delete from User where User_id = :id");
		query.setLong("id", id);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAllUser() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.list();
	}

	@Override
	public User findUserById(long id) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user_id", id));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findByEmail(String email) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("email", email));
		return (User) criteria.uniqueResult();
	}
}