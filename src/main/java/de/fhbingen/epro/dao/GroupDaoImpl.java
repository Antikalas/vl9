package de.fhbingen.epro.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import de.fhbingen.epro.model.Group;


@Repository("GroupDao")
public class GroupDaoImpl extends AbstractDao<Integer, Group> implements GroupDao {

	@Override
	public Group findById(long id) {
		return getByKey(id);
	}

	@Override
	public void saveGroup(Group group) {
		persist(group);
	}


	@Override
	public void deleteGroupById(long id) {
		Query query = getSession().createSQLQuery("delete from Group where Group_id = :id");
		query.setLong("id", id);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Group> findAllGroup() {
		Criteria criteria = createEntityCriteria();
		return (List<Group>) criteria.list();
	}

	@Override
	public Group findGroupById(long id) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("group_id", id));
		return (Group) criteria.uniqueResult();
	}
}