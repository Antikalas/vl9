package de.fhbingen.epro.dao;

import java.util.List;

import de.fhbingen.epro.model.User;


public interface UserDao {

	User findById(long id);
	
	User findByEmail(String email);

	void saveUser(User user);
	
	void deleteUserById(long id);
	
	List<User> findAllUser();

	User findUserById(long id);

}
