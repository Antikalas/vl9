package de.fhbingen.epro.dao;


import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import de.fhbingen.epro.model.Right;



@Repository("RightDao")
public class RightDaoImpl extends AbstractDao<Integer, Right> implements RightDao {

	@Override
	public Right findById(long id) {
		return getByKey(id);
	}

	@Override
	public void saveRight(Right right) {
		persist(right);
	}


	@Override
	public void deleteRightById(long id) {
		Query query = getSession().createSQLQuery("delete from Right where Right_id = :id");
		query.setLong("id", id);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Right> findAllRight() {
		Criteria criteria = createEntityCriteria();
		return (List<Right>) criteria.list();
	}

	@Override
	public Right findRightById(long id) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("right_id", id));
		return (Right) criteria.uniqueResult();
	}
}