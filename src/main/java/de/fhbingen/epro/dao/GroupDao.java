package de.fhbingen.epro.dao;

import java.util.List;

import de.fhbingen.epro.model.Group;


public interface GroupDao {

	Group findById(long id);

	void saveGroup(Group group);
	
	void deleteGroupById(long id);
	
	List<Group> findAllGroup();

	Group findGroupById(long id);

}
