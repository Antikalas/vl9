package de.fhbingen.epro.repos;


import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import de.fhbingen.epro.model.User;


public interface UserRepo extends CrudRepository<User, Long>{
	@RestResource(path="byEmail") 
	public User findByEmail(@Param("email") String email);
	@RestResource(path="byId") 
	public User findById(@Param("id") long id);
	
	
}
