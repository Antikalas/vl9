package de.fhbingen.epro.repos;

import org.springframework.data.repository.CrudRepository;

import de.fhbingen.epro.model.Group;


public interface GroupRepo extends CrudRepository<Group, Long>{

}
