package de.fhbingen.epro.repos;

import org.springframework.data.repository.CrudRepository;

import de.fhbingen.epro.model.Right;



public interface RightRepo extends CrudRepository<Right, Long>{

}
