package de.fhbingen.epro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhbingen.epro.dao.RightDao;
import de.fhbingen.epro.model.Right;


@Service("rightService")
@Transactional
public class RightServiceImpl implements RightService {

	@Autowired
	private RightDao dao;
	
	public Right findById(long id) {
		return dao.findById(id);
	}

	public void saveRight(Right right) {
		dao.saveRight(right);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateRight(Right right) {
		Right entity = dao.findById(right.getId());
		if(entity!=null){
			entity.setName(right.getName());
			entity.setId(right.getId());
			entity.setKey(right.getKey());
			entity.setDescription(right.getDescription());
		}
	}

	public void deleteserById(int id) {
		dao.deleteRightById(id);
	}
	
	public List<Right> findAllRight() {
		return dao.findAllRight();
	}

	@Override
	public void deleteRightById(long id) {
		dao.deleteRightById(id);
	}
	
}
