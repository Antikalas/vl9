package de.fhbingen.epro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.fhbingen.epro.dao.GroupDao;
import de.fhbingen.epro.model.Group;


@Service("groupService")
@Transactional
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao dao;
	
	public Group findById(long id) {
		return dao.findById(id);
	}

	public void saveGroup(Group group) {
		dao.saveGroup(group);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateGroup(Group group) {
		Group entity = dao.findById(group.getId());
		if(entity!=null){
			entity.setName(group.getName());
			entity.setRight(group.getRight());
			entity.setId(group.getId());
			entity.setName(group.getName());
			entity.setDescription(group.getDecription());
		}
	}

	public void deleteserById(long id) {
		dao.deleteGroupById(id);
	}
	
	public List<Group> findAllGroup() {
		return dao.findAllGroup();
	}

	@Override
	public void deleteGroupById(long id) {
		dao.deleteGroupById(id);
	}
	
}
