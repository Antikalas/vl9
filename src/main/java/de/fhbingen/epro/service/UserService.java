package de.fhbingen.epro.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import de.fhbingen.epro.model.User;


public interface UserService {

	User findById(long id);
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteUserById(long id);

	List<User> findAllUser(); 
	
	User findByEmail(String email);
}
