package de.fhbingen.epro.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import de.fhbingen.epro.dao.UserDao;
import de.fhbingen.epro.model.User;


@Service("UserService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;
	
	public User findById(long id) {
		return dao.findById(id);
	}

	public void saveUser(User user) {
		dao.saveUser(user);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateUser(User user) {
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setName(user.getName());
			entity.setFirstname(user.getFirstname());
			entity.setEmail(user.getEmail());
			entity.setPassword(user.getPassword());
			entity.setGroup(user.getGroup());
		}
	}

	public void deleteserById(int id) {
		dao.deleteUserById(id);
	}
	
	public List<User> findAllUser() {
		return dao.findAllUser();
	}

	@Override
	public void deleteUserById(long id) {
		dao.deleteUserById(id);
	}

	@Override
	public User findByEmail(String email) {
		return dao.findByEmail(email);
	}
}
