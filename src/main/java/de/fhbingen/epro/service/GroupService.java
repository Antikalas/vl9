package de.fhbingen.epro.service;

import java.util.List;

import de.fhbingen.epro.model.Group;


public interface GroupService {

	Group findById(long id);
	
	void saveGroup(Group group);
	
	void updateGroup(Group group);
	
	void deleteGroupById(long id);

	List<Group> findAllGroup(); 
	
}
