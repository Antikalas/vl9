package de.fhbingen.epro.service;

import java.util.List;

import de.fhbingen.epro.model.Right;


public interface RightService {

	Right findById(long id);
	
	void saveRight(Right right);
	
	void updateRight(Right right);
	
	void deleteRightById(long id);

	List<Right> findAllRight(); 
	
}
