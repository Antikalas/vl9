package de.fhbingen.epro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class JSPController {

  @RequestMapping("/jsp/{sub1}/{sub2}")
    public String sub2(ModelAndView modelAndView, @PathVariable String sub1,@PathVariable String sub2) {
        return sub1+"/"+sub2;
  }
  
  @RequestMapping("/jsp/{sub1}")
  public String sub1(ModelAndView modelAndView, @PathVariable String sub1) {
      return sub1;
}
  @RequestMapping("/jsp")
  public String index(ModelAndView modelAndView) {
      return "index";
}
        
}