package de.fhbingen.epro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.fhbingen.epro.model.User;
import de.fhbingen.epro.service.UserService;




@RestController
@RequestMapping("/user")
public class UserRestController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	void saveUser(@RequestBody User user) {
		try {
			userService.saveUser(user);
		} catch (Exception e) {
			 e.printStackTrace();
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody
	User findById(@PathVariable("id") long id) {
		User user = null;
		try {
			user = userService.findById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<User> getUsers() {

		List<User> userList = null;
		try {
			userList = userService.findAllUser();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return userList;
	}

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public @ResponseBody
	void deleteUser(@PathVariable("id") long id) {

		try {
			userService.deleteUserById(id);
		} catch (Exception e) {
			//return new Status(0, e.toString());
			e.printStackTrace();
		}

	}
}
