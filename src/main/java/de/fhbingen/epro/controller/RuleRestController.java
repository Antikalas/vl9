package de.fhbingen.epro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.fhbingen.epro.model.Right;
import de.fhbingen.epro.service.RightService;





@RestController
@RequestMapping("/right")
public class RuleRestController {

	@Autowired
	RightService rightService;

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	void saveRight(@RequestBody Right right) {
		try {
			rightService.saveRight(right);
		} catch (Exception e) {
			 e.printStackTrace();
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Right findById(@PathVariable("id") long id) {
		Right right = null;
		try {
			right = rightService.findById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return right;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Right> getRights() {

		List<Right> rightList = null;
		try {
			rightList = rightService.findAllRight(); 
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rightList;
	}

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public @ResponseBody
	void deleteGroup(@PathVariable("id") long id) {

		try {
			rightService.deleteRightById(id);
		} catch (Exception e) {
			//return new Status(0, e.toString());
			e.printStackTrace();
		}

	}
}
