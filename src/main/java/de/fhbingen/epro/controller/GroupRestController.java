package de.fhbingen.epro.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.fhbingen.epro.model.Group;
import de.fhbingen.epro.service.GroupService;






@RestController
@RequestMapping("/group")
public class GroupRestController {

	@Autowired
	GroupService groupService;


	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	void saveRight(@RequestBody Group group) {
		try {
			groupService.saveGroup(group);
		} catch (Exception e) {
			 e.printStackTrace();
		}

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public @ResponseBody
	Group findById(@PathVariable("id") long id) {
		Group group= null;
		try {
			group = groupService.findById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return group;
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody
	List<Group> getRights() {

		List<Group> groupList = null;
		try {
			groupList = groupService.findAllGroup(); 
		} catch (Exception e) {
			e.printStackTrace();
		}

		return groupList;
	}

	@RequestMapping(value = "delete/{id}", method = RequestMethod.GET)
	public @ResponseBody
	void deleteGroup(@PathVariable("id") long id) {

		try {
			groupService.deleteGroupById(id);
		} catch (Exception e) {
			//return new Status(0, e.toString());
			e.printStackTrace();
		}

	}
}
