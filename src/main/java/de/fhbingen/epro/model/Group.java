package de.fhbingen.epro.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name="Groups")
public class Group {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "GROUP_ID")
	private long id;

	@Size(min=3, max=50)
	@Column(name = "NAME", nullable = false, unique = true)
	private String name;

	@NotNull
	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@OneToMany(mappedBy="group")
	 private Set<User> user=new HashSet<User>(0);
	
	 @ManyToMany
	 @JoinTable(name = "group_right",
	            joinColumns = @javax.persistence.JoinColumn(name = "GROUP_ID"),
	            inverseJoinColumns = @javax.persistence.JoinColumn(name = "RIGHT_ID"))
	    private Set<Right> right;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDecription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Right> getRight() {
		return right;
	}

	public void setRight(Set<Right> right) {
		this.right = right;
	}


}
